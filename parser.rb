require 'json'
require 'httparty'

orderJSON = {
  "id": 9987071,
  "store_id": 282,
  "date_created": "2019-06-24T16:45:32.000-04:00",
  "date_closed": "2019-06-24T16:45:35.000-04:00",
  "last_updated": "2019-06-25T13:26:49.000-04:00",
  "total_amount": 49.9,
  "total_shipping": 5.14,
  "total_amount_with_shipping": 55.04,
  "paid_amount": 55.04,
  "expiration_date": "2019-07-22T16:45:35.000-04:00",
  "order_items": [
    {
      "item": {
        "id": "IT4801901403",
        "title": "Produto de Testes"
      },
      "quantity": 1,
      "unit_price": 49.9,
      "full_unit_price": 49.9
    }
  ],
  "payments": [
    {
      "id": 12312313,
      "order_id": 9987071,
      "payer_id": 414138,
      "installments": 1,
      "payment_type": "credit_card",
      "status": "paid",
      "transaction_amount": 49.9,
      "taxes_amount": 0,
      "shipping_cost": 5.14,
      "total_paid_amount": 55.04,
      "installment_amount": 55.04,
      "date_approved": "2019-06-24T16:45:35.000-04:00",
      "date_created": "2019-06-24T16:45:33.000-04:00"
    }
  ],
  "shipping": {
    "id": 43444211797,
    "shipment_type": "shipping",
    "date_created": "2019-06-24T16:45:33.000-04:00",
    "receiver_address": {
      "id": 1051695306,
      "address_line": "Rua Fake de Testes 3454",
      "street_name": "Rua Fake de Testes",
      "street_number": "3454",
      "comment": "teste",
      "zip_code": "85045020",
      "city": {
        "name": "Cidade de Testes"
      },
      "state": {
        "name": "São Paulo"
      },
      "country": {
        "id": "BR",
        "name": "Brasil"
      },
      "neighborhood": {
        "id": nil,
        "name": "Vila de Testes"
      },
      "latitude": -23.629037,
      "longitude": -46.712689,
      "receiver_phone": "41999999999"
    }
  },
  "status": "paid",
  "buyer": {
    "id": 136226073,
    "nickname": "JOHN DOE",
    "email": "john@doe.com",
    "phone": {
      "area_code": 41,
      "number": "999999999"
    },
    "first_name": "John",
    "last_name": "Doe",
    "billing_info": {
      "doc_type": "CPF",
      "doc_number": "09487965477"
    }
  }
}.to_json

order = JSON.parse orderJSON

# iterate through items array to create a array of hashes to send on request

items = []

order["order_items"].map do |item|
   items << {
     "externalCode": item["item"]["id"],
     "name": item["item"]["title"],
     "price": item["unit_price"],
     "quantity": item["quantity"],
     "total": item["full_unit_price"],
     "subItems": item["sub_items"] || nil
   }
end

# iterate through payments array to create a array of hashes to send on request

payments = []

order["payments"].map do |payment|
   payments << {
     "type": payment["payment_type"].upcase,
     "value": payment["total_paid_amount"]
   }
end

# interpolate to create contact

contact = "#{order['buyer']['phone']['area_code']}#{order['buyer']['phone']['number']}"

# create final parameters to send

params = {
  "externalCode": order["id"],
  "storeId": order["store_id"],
  "subTotal": order["total_amount"],
  "deliveryFee": order["total_shipping"],
  "total_shipping": order["total_shipping"],
  "total": order["total_amount_with_shipping"],
  "postalCode": order["shipping"]["receiver_address"]["zip_code"],
  "country": order["shipping"]["receiver_address"]["country"]["id"],
  "state": order["shipping"]["receiver_address"]["state"]["name"],
  "city": order["shipping"]["receiver_address"]["city"]["name"],
  "district": order["shipping"]["receiver_address"]["neighborhood"]["name"],
  "street": order["shipping"]["receiver_address"]["street_name"],
  "number": order["shipping"]["receiver_address"]["street_number"],
  "complement": order["shipping"]["receiver_address"]["comment"],
  "latitude": order["shipping"]["receiver_address"]["latitude"],
  "longitude":  order["shipping"]["receiver_address"]["longitude"],
  "dtOrderCreate": order["date_created"],
  "customer": {
      "externalCode": order["buyer"]["id"],
      "name": order["buyer"]["nickname"],
      "email": order["buyer"]["email"],
      "contact": contact
  },
  "items": items,
  "payments": payments
}.to_json rescue "Algum dado eh invalido ou nao existe"

# create header

headers = {
      "X-Sent" => "#{Time.now.strftime("%Hh%M - %d/%m/%y")}"
    }

response = HTTParty.post(
  "https://delivery-center-recruitment-ap.herokuapp.com/",
  :body => params,
  :headers => headers
)

puts "Response Code: #{response.code}"
puts "Response: #{response.parsed_response}"
