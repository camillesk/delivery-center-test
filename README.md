## Delivery Center Ruby Back-end Test
Data: 05/04/20

O arquivo tem como objetivo parsear um objeto JSON definido 'hardcoded' internamente, fazer alguns tratamentos e envia-lo para a url *https://delivery-center-recruitment-ap.herokuapp.com/* seguindo os critérios do teste.

Para rodar basta digitar no terminal:
> ruby parser.rb
